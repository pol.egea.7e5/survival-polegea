﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public KayaHealth playerHealth;       
    public float restartDelay = 5f;           


    Animator anim;                          
    float restartTimer;                     


    void Awake()
    {
        Cursor.visible = false;
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");

            restartTimer += Time.deltaTime;

            if (restartTimer >= restartDelay)
                Cursor.lockState = CursorLockMode.Confined; Cursor.visible = true;
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }
}
