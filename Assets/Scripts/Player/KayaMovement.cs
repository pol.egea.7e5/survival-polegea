﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KayaMovement : MonoBehaviour
{
    public float speed = 6f;
    public float startingSpeed = 6f;
    public CharacterController controller;
    public Transform cam;
    public float startingEnergy = 100;
    public float currentEnergy = 100;
    public Slider EnergySlider;
    public float turnSmoothtime = 0.1f;
    float turnSmoothVelocity;

    bool hasSpeedPowerup = false;
    bool isRunning;

    //animiation
    Animator anim;
    Rigidbody rBody;
    int floorMask;

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        isRunning = false;
        currentEnergy = startingEnergy;
        floorMask = LayerMask.GetMask("Floor");

        //Animiation
        anim = GetComponent<Animator>();
        checkRigidBody();
    }

    private void Update()
    {

        if (((Input.GetKeyDown(KeyCode.LeftShift)) && currentEnergy >= 25 && !hasSpeedPowerup && !isRunning))
        {
            isRunning = true;
            currentEnergy -= 25;
            EnergySlider.value = currentEnergy;
            speed = 16;
            Invoke("resetSpeed", 1);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift) && !hasSpeedPowerup)
            resetSpeed();

        currentEnergy += 3 * Time.deltaTime;

        setSliderBar();
    }

    void FixedUpdate()
    {
        Move();
        Animiating(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }


    void Move()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothtime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            controller.Move(moveDir.normalized * speed * Time.deltaTime);
        }
    }
    void checkRigidBody()
    {
        if (GetComponent<Rigidbody>())
            rBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("Add rigidbody to the object");
    }

    void Animiating(float h, float v)
    {
        bool moving = h != 0f || v != 0f;

        anim.SetBool("IsMoving", moving);
    }

    public void powerUpSpeed(float duration)
    {
        hasSpeedPowerup = true;
        currentEnergy = startingEnergy;
        EnergySlider.value = currentEnergy;
        float speedMultiplier = Random.Range(1, 3);

        speed *= speedMultiplier;
        Invoke("resetSpeed", duration);
    }

    private void resetSpeed()
    {
        speed = startingSpeed;
        hasSpeedPowerup = false;
        isRunning = false;
    }

    void setSliderBar()
    {
        EnergySlider.value = currentEnergy;
    }
}
